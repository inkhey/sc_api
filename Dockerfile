FROM python:3.10

WORKDIR /run
COPY ./requirements.txt /run/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /run/requirements.txt
COPY ./api /run/api

CMD ["uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "80"]