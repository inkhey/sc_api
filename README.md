# Simple Cache Api from Sens Critique Website

You need :
- docker
- docker-compose
- wget/web-browser/httpie 

## Running

Build docker image:

````bash
docker build -t sc_api .
````

Trick to resolve all local containers hostnames on host computer (useful for testing):

```bash
docker run --name dns_docker_solver --hostname dns.mageddo --restart=unless-stopped -p 5380:5380 -v /var/run/docker.sock:/var/run/docker.sock -v /etc/resolv.conf:/etc/resolv.conf defreitas/dns-proxy-server
```

Run the Api service:

```bash
docker-compose up
wget http://test.senscritique.local
```

Testing the api using httpie:
```bash
$ http get  http://test.senscritique.local/product/film/godland/47402147
HTTP/1.1 200 OK
content-length: 4
content-type: application/json
date: Sun, 25 Dec 2022 12:32:15 GMT
server: uvicorn

null


$ http post  http://test.senscritique.local/product/film/godland/47402147
HTTP/1.1 200 OK
content-length: 4
content-type: application/json
date: Sun, 25 Dec 2022 12:32:19 GMT
server: uvicorn

null


$ http get  http://test.senscritique.local/product/film/godland/47402147
HTTP/1.1 200 OK
content-length: 70
content-type: application/json
date: Sun, 25 Dec 2022 12:32:20 GMT
server: uvicorn

{
    "category": "film",
    "item_id": 47402147,
    "rating": "7.2",
    "slug": "godland"
}
````

Cleanup redis database (set container_id to sc_api container id):

```bash
docker exec -it -w /run/api $CONTAINER_ID python main.py cleanup-database
```

Access to the API documentation: 
```
xdg-open http://test.senscritique.local/docs
```

## Pistes d’améliorations/Limites:

Voici quelques points qu’il me semble utile de notifier.

À noter : il s’agit là d’un environnement de dev/test, dans un contexte de production utilisant docker-compose, des ajustements de sécurité
sur les conteneurs serait envisageable (mot de passe redis par exemple).

### Choix techniques:

- fastapi : Framework d’api python moderne (supportant ASGI et avec documentation auto-générée) et populaire.
- click/requests : librairie simple et populaire en python.
- docker-compose : plus simple de gérer la combinaison redis/python avec.

### Limites technique:

- Utilisation de scraping, incertitude sur la durée de vie de la méthodologie. Utiliser un vrai api
ou une librairie opensource de scraping supportant sens-critique pourrait être une bonne idée pour limiter les risques : peut-être https://woob.tech/modules#mod_senscritique ?
- Identifiant composite en 3 parties pour des raisons de praticité qui ne me semble pas forcément complètement raccord avec la spécification initiale prévue. Pas trouvé jusqu'à présent d'autre solutions.
- Récupération du strict minimum d’informations sur l’œuvre.
- Pas de gestion des cas d'erreurs

### Amélioration de la qualité:

- Ajout de tests automatisés d’api voir d’une CI.
- Ajout de mécanisme de bonne pratiques : linter, formatter, precommit, vérification du typage, etc.
- Meilleure gestion des versions des paquets python utilisés : versions fixés strictement dans le requirements.txt pour éviter les problèmes.
- Changer le hostname du conteneur test.senscritique.local par une autre extension, ".local" est lié au protocole MDNS et peut causer des conflits : https://www.rfc-editor.org/rfc/rfc6762#appendix-G .
- Creuser un peu la doc de fastapi pour rendre la documentation openapi plus précise.