import json
from typing import Optional

import click
import requests
from fastapi import FastAPI
from pydantic import BaseModel
from redis import Redis
from bs4 import BeautifulSoup

app = FastAPI(debug=True)


class Product(BaseModel):
    category: str
    slug: str
    item_id: int
    rating: str


def get_redis_connection() -> Redis:
    return Redis(host='cache', port=6379, db=0)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/product/{category}/{slug}/{item_id}", response_model=Product)
def get_item(category: str, slug: str, item_id: str) -> Optional[Product]:
    redis = get_redis_connection()
    data = redis.get(item_id)
    if data:
        data = json.loads(data)
    return data


@app.post("/product/{category}/{slug}/{item_id}")
def post_item(category: str, slug: str, item_id: str) -> None:
    redis = get_redis_connection()
    with requests.get(
        "https://www.senscritique.com/{category}/{slug}/{item_id}".format(
            category=category,
            slug=slug,
            item_id=item_id
        )
    ) as result:
        soup = BeautifulSoup(result.text, 'html.parser')
        rating = soup.find('div', attrs={'data-testid': 'Rating'}).text
        redis.set(item_id, json.dumps(
            {
                'category': category,
                'slug': slug,
                'item_id': item_id,
                'rating': rating
             }
            )
        )


@click.command()
def cleanup_database():
    click.echo("Cleanup database")
    redis = get_redis_connection()
    redis.flushdb()
    click.echo("done")


@click.group()
def cli():
    pass


cli.add_command(cleanup_database)

if __name__ == '__main__':
    cli()
